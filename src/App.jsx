import { useEffect, useState } from "react"

function App() {
  const [count, setCount] = useState(0)

  const click = () => {
    setCount(count + 1)
  }

  useEffect(() => {
    if(localStorage.getItem("count") !== null) {
      setCount(localStorage.getItem("count"))
    }
  }, [])

  return (
    <div className="container flex justify-evenly items-center flex-col h-screen">
      <p className="font-bold text-sm sm:text-base">Anda telah menekan tombol sebanyak</p>
      <div className="p-3 font-bold flex flex-col items-center w-2/5 rounded-xl h-48 justify-center">
        <h1 className="text-6xl drop-shadow-xl">{count}</h1>
      </div>
      <button className='bg-black text-xs sm:text-base sm:w-2/5 text-white py-3 px-6 rounded-full font-bold active:bg-white active:text-black focus:outline-none focus:ring focus:ring-black transition' onClick={() => click()}>
        Tekan tombol ini jika bosan!
      </button>
      <p className="font-bold">Author : <a href="https://www.instagram.com/salman.almajali/" target="_blank">@salman.almajali</a></p>
    </div>
  )
}

export default App
